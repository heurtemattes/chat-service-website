
<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->

# Eclipse Projects Space (#eclipse-projects:matrix.eclipse.org)
Welcome to the Eclipse projects space! This space is dedicated to discussions and collaboration around projects developed under the Eclipse Foundation.We encourage active participation and constructive discussions. Please feel free to share your ideas, ask for advice, provide feedback, report issues... This space is a community hub for Eclipse-related projects, and we welcome all perspectives and contributions.To ensure a respectful environment, please be aware of our [privacy policy](https://www.eclipse.org/legal/privacy.php) and follow our [code of conduct](https://www.eclipse.org/org/documents/Community_Code_of_Conduct.php) and `guidelines`. Full documentation can be found [here](https://chat.eclipse.org/docs/) and [FAQ](https://chat.eclipse.org/docs/faq)
## kiso testing committer Room (#kiso-testing-committer:matrix.eclipse.org)
Strategy discussions regarding pykiso (roadmap, next features, next PRs to merge)
## kiso testing contributor Room (#kiso-testing-contributor:matrix.eclipse.org)
Technical discussions regarding pykiso and the different contributions
## Eclipse SUMO™ Room (#automotive.sumo:matrix.eclipse.org)
General discussions about the Eclipse SUMO (Simulation of Urban MObility) project
## Eclipse Automotive Room (#automotive:matrix.eclipse.org)
The Eclipse Automotive Top-Level Project provides a space for open source projects to explore ideas and technologies addressing challenges in the automotive, mobility and transportation domain. It is the common goal of this project to provide tools and composable building blocks to empower the development of solutions for the mobility of the future.
## Eclipse Tractus-X Room (#tools.tractus-x:matrix.eclipse.org)
Eclipse Tractus-X™ aims at the development of systems in support of the Catena-X data space, with focus on PLM / quality, resiliency, sustainability, and shared network services.
## Eclipse Velocitas Room (#velocitas:matrix.eclipse.org)
Eclipse Velocitas provides an end-to-end, scalable, modular and open source development toolchain for creating containerized and non-containerized in-vehicle applications.
## Eclipse Tractus-X SSI Room (#tools.tractus-x-ssi:matrix.eclipse.org)
Eclipse Tractus-X™ component SSI
## Eclipse Packager Room (#packager:matrix.eclipse.org)
The Eclipse Packager™ project offers a set of core functionality to work with RPM and Debian package files in plain Java. This functionality is offered in simple JAR variant to create your own solutions, or in ready-to-run tools like an Apache Maven plugin.
## Eclipse 4diac IDE Room (#4diac-ide:matrix.eclipse.org)
Questions and discussions targeting 4diac IDE.
## Eclipse 4diac Town Square Room (#4diac-townsquare:matrix.eclipse.org)
A town square for the Eclipse 4diac project, the place for conversations that do not fit any dedicated Matrix room.
## Jakarta RESTful Web Services™ Room (#ee4j.rest:matrix.eclipse.org)
Jakarta RESTful Web Services™ provides a specification document, TCK and foundational API to develop web services following the Representational State Transfer (REST) architectural pattern. JAX-RS: Java API for RESTful Web Services (JAX-RS) is a Java programming language API spec that provides support in creating web services according to the Representational State Transfer (REST) architectural pattern.
## Eclipse Theia Room (#ecd.theia:matrix.eclipse.org)
Eclipse Theia™ is an extensible platform to develop full-fledged, multi-language, cloud & desktop IDE-like products with state-of-the-art web technologies.
## Eclipse 4diac FORTE Room (#4diac-forte:matrix.eclipse.org)
Questions and discussions targeting 4diac FORTE.
## Eclipse Mylyn Room (#tools.mylyn:matrix.eclipse.org)
Eclipse Mylyn is a Task-Focused Interface for Eclipse that reduces information overload and makes multi-tasking easy. The mission of the Mylyn project is to provide: 1. Frameworks and APIs for Eclipse-based task and Application Lifecycle Management (ALM) 2. Exemplary tools for task-focused programming within the Eclipse IDE. 3. Reference implementations for open source ALM tools used by the Eclipse community and for open ALM standards such as OSLC The project is structured into sub-projects, each representing an ALM category and providing common APIs for specific ALM tools. The primary consumers of this project are ALM ISVs and other adopters of Eclipse ALM frameworks.  Please see the project charter for more details. Mylyn makes tasks a first class part of Eclipse, and integrates rich and offline editing for repositories such as Bugzilla, Trac, and JIRA. Once your tasks are integrated, Mylyn monitors your work activity to identify information relevant to the task-at-hand, and uses this task context to focus the Eclipse UI on the interesting information, hide the uninteresting, and automatically find what's related. This puts the information you need to get work done at your fingertips and improves productivity by reducing searching, scrolling, and navigation. By making task context explicit Mylyn also facilitates multitasking, planning, reusing past efforts, and sharing expertise.
## Jakarta MVC™ Room (#ee4j.mvc:matrix.eclipse.org)
Jakarta Model-View-Controller, or Jakarta MVC™ for short, is a common pattern in Web frameworks where it is used predominantly to build HTML applications. The model refers to the application’s data, the view to the application’s data presentation and the controller to the part of the system responsible for managing input, updating models and producing output. Web UI frameworks can be categorized as action-based or component-based. In an action-based framework, HTTP requests are routed to controllers where they are turned into actions by application code; in a component-based framework, HTTP requests are grouped and typically handled by framework components with little or no interaction from application code. In other words, in a component-based framework, the majority of the controller logic is provided by the framework instead of the application.
## Oniro Project Room (#oniro-project:matrix.eclipse.org)
A town square for Oniro Project, the place for conversations that do not fit any dedicated Matrix room.
## Oniro IDE Room (#oniro-ide:matrix.eclipse.org)
Development of IDE for Oniro.
## Oniro OH Room (#oniro-oh:matrix.eclipse.org)
Development of Eclipse Oniro OH Project.
## Eclipse GEMOC Studio Room (#gemoc-studio:matrix.eclipse.org)
The Eclipse GEMOC Studio is a language workbench for designing and integrating EMF-based modeling languages. It gives a special focus on behavior integration by providing a framework with a generic interface to plug in different execution engines!
## Eclipse Krazo™ Room (#ee4j.krazo:matrix.eclipse.org)
Eclipse Krazo™ is an implementation of action-based MVC specified by MVC 1.0 (JSR-371). It builds on top of JAX-RS and currently contains support for RESTEasy, Jersey and CXF with a well-defined SPI for other implementations.
## Eclipse Tractus-X EDC Room (#tools.tractusx-edc:matrix.eclipse.org)
'The Tractus-X EDC provides pre-built control- and data-plane docker images and helm charts of the Eclipse DataSpaceConnector Project.'
## Eclipse Xtext Room (#xtext:matrix.eclipse.org)
Eclipse Xtext
## Adoptium General Room (#adoptium-general:matrix.eclipse.org)
https://github.com/adoptium/adoptium - starting point for build farm folkshttps://drive.google.com/drive/folders/1jjdXw3fAgRIeupb_bXCoOgFH9_iyo1ak - meeting agendas / minutes
## Adoptium Build Room (#adoptium-build:matrix.eclipse.org)
Discussions related to the building of anything in the Adoptium machine farmNightly triage: https://docs.google.com/document/d/1vcZgHJeR8rW8U8OD23Uob7A1dbLrtkURZUkinUp7f_w/edit?usp=sharing
## Adoptium Containers Room (#adoptium-containers:matrix.eclipse.org)
Topics relating to containers
## Adoptium Installer Room (#adoptium-installer:matrix.eclipse.org)
Topics relating to installer
## Adoptium Testing Aqavit Room (#adoptium-testing-aqavit:matrix.eclipse.org)
Topics relating to testing activities done as part of the activities under the AQAvit project
## Adoptium Release Room (#adoptium-release:matrix.eclipse.org)
Topics relating to release: https://github.com/adoptium/temurin-build/blob/master/RELEASING.md
## Adoptium Infrastructure Room (#adoptium-infrastructure:matrix.eclipse.org)
Discussion relating to the machines and services supporting OpenJDK build & test. Machine list @ https://github.com/adoptium/infrastructure/blob/master/ansible/inventory.yml
## Adoptium Secure Dev Room (#adoptium-secure-dev:matrix.eclipse.org)
Topics relating to Secure Dev: https://docs.google.com/document/d/1bxYCEbM4Wn2uLl_lgY67a6x5VBWH_ZGdLwG_yDByfT0/edit?pli=1#heading=h.bti4iq4qm4f0
# Eclipse Foundation Space (#eclipsefdn:matrix.eclipse.org)
Open communication channel at Eclipse Foundation
## Chat Service moderation Room (#eclipsefdn.chat-moderation:matrix.eclipse.org)
Room moderation with mjolnir for chat service
## IT Room (#eclipsefdn.it:matrix.eclipse.org)
Welcome to the IT team room! This room is a space for the Eclipse Foundation IT staff to discuss, report, and troubleshoot technical issues related to our organization's infrastructure and systems. And don't forget to report issues via [Eclipse HelpDesk](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/new)
## chat service support Room (#eclipsefdn.chat-support:matrix.eclipse.org)
Welcome to the chat support room! This room is to provide help and assistance with our chat service. Feel free to ask questions, report issues, or share feedback. Full FAQ is available [here](https://chat.eclipse.org/docs/faq/)
## IP License compliance Room (#eclipsefdn.ip-license-compliance:matrix.eclipse.org)
Welcome to the IP license compliance room!
## general Room (#eclipsefdn.general:matrix.eclipse.org)
Welcome to the general room! This room is a place for users to chat and discuss a wide range of topics about the chat service, the foundation, Eclipse projects, ECA, contribution, ...Feel free to introduce yourself, share your interests, and start ask questions. Full Documentation is available [here](https://chat.eclipse.org/docs/)Want new rooms/spaces, please follow intructions [here](https://chat.eclipse.org/docs/faq/#request-for-roomspace)
## Chat Service Slack Admin Room (null)
null
# Oniro Space (#oniro:matrix.eclipse.org)
The mission of the Eclipse Oniro is the design, development, production and maintenance of an open source software platform, having an operating system, an ADK/SDK, standard APIs and basic applications, like UI, as core elements, targeting different industries thanks to a next-generation multi-kernel architecture, that simplifies the existing landscape of complex systems, and its deployment across a wide range of devices.
## Oniro OH Room (#oniro-oh:matrix.eclipse.org)
Development of Eclipse Oniro OH Project.
## Oniro IDE Room (#oniro-ide:matrix.eclipse.org)
Development of IDE for Oniro.
## Oniro Project Room (#oniro-project:matrix.eclipse.org)
A town square for Oniro Project, the place for conversations that do not fit any dedicated Matrix room.
# Eclipse 4diac™ Space (#eclipse-4diac:matrix.eclipse.org)
Eclipse 4diac™ provides an open source infrastructure for distributed industrial process measurement and control systems based on the IEC 61499 standard.
## Eclipse 4diac IDE Room (#4diac-ide:matrix.eclipse.org)
Questions and discussions targeting 4diac IDE.
## Eclipse 4diac FORTE Room (#4diac-forte:matrix.eclipse.org)
Questions and discussions targeting 4diac FORTE.
## Eclipse 4diac Town Square Room (#4diac-townsquare:matrix.eclipse.org)
A town square for the Eclipse 4diac project, the place for conversations that do not fit any dedicated Matrix room.
# Jakarta RESTful Web Services™ Space (#jaxrs:matrix.eclipse.org)
Jakarta RESTful Web Services™ provides a specification document, TCK and foundational API to develop web services following the Representational State Transfer (REST) architectural pattern. JAX-RS: Java API for RESTful Web Services (JAX-RS) is a Java programming language API spec that provides support in creating web services according to the Representational State Transfer (REST) architectural pattern.
## Jakarta RESTful Web Services™ Room (#ee4j.rest:matrix.eclipse.org)
Jakarta RESTful Web Services™ provides a specification document, TCK and foundational API to develop web services following the Representational State Transfer (REST) architectural pattern. JAX-RS: Java API for RESTful Web Services (JAX-RS) is a Java programming language API spec that provides support in creating web services according to the Representational State Transfer (REST) architectural pattern.
# kiso testing Space (#kiso-testing:matrix.eclipse.org)
Space to discuss pykiso topics
## kiso testing contributor Room (#kiso-testing-contributor:matrix.eclipse.org)
Technical discussions regarding pykiso and the different contributions
## kiso testing committer Room (#kiso-testing-committer:matrix.eclipse.org)
Strategy discussions regarding pykiso (roadmap, next features, next PRs to merge)
# Eclipse Adoptium™ Space (#adoptium:matrix.eclipse.org)
The mission of the Eclipse Adoptium Top-Level Project is to produce high-quality runtimes and associated technology for use within the Java ecosystem.  We achieve this through a set of Projects under the Adoptium PMC and a close working partnership with external projects, most notably OpenJDK for providing the Java SE runtime implementation.  Our goal is to meet the needs of both the Eclipse community and broader runtime users by providing a comprehensive set of technologies around runtimes for Java applications that operate alongside existing standards, infrastructures, and cloud platforms.The AdoptOpenJDK project was established in 2017 following years of discussions about the general lack of an open and reproducible build and test system for OpenJDK source across multiple platforms.  Since then it has grown to become a leading provider of high-quality OpenJDK-based binaries used by enterprises in embedded systems, desktops, traditional servers, modern cloud platforms, and large mainframes.  The Eclipse Adoptium project is the continuation of the original AdoptOpenJDK mission.
## Adoptium General Room (#adoptium-general:matrix.eclipse.org)
https://github.com/adoptium/adoptium - starting point for build farm folkshttps://drive.google.com/drive/folders/1jjdXw3fAgRIeupb_bXCoOgFH9_iyo1ak - meeting agendas / minutes
## Adoptium Build Room (#adoptium-build:matrix.eclipse.org)
Discussions related to the building of anything in the Adoptium machine farmNightly triage: https://docs.google.com/document/d/1vcZgHJeR8rW8U8OD23Uob7A1dbLrtkURZUkinUp7f_w/edit?usp=sharing
## Adoptium Containers Room (#adoptium-containers:matrix.eclipse.org)
Topics relating to containers
## Adoptium Installer Room (#adoptium-installer:matrix.eclipse.org)
Topics relating to installer
## Adoptium Release Room (#adoptium-release:matrix.eclipse.org)
Topics relating to release: https://github.com/adoptium/temurin-build/blob/master/RELEASING.md
## Adoptium Secure Dev Room (#adoptium-secure-dev:matrix.eclipse.org)
Topics relating to Secure Dev: https://docs.google.com/document/d/1bxYCEbM4Wn2uLl_lgY67a6x5VBWH_ZGdLwG_yDByfT0/edit?pli=1#heading=h.bti4iq4qm4f0
## Adoptium Testing Aqavit Room (#adoptium-testing-aqavit:matrix.eclipse.org)
Topics relating to testing activities done as part of the activities under the AQAvit project
## Adoptium Infrastructure Room (#adoptium-infrastructure:matrix.eclipse.org)
Discussion relating to the machines and services supporting OpenJDK build & test. Machine list @ https://github.com/adoptium/infrastructure/blob/master/ansible/inventory.yml
