#!/bin/bash

[[ -n "$CI_DEBUG" ]] && set -x

MATRIX_SERVER="https://matrix.eclipse.org"
#MATRIX_ACCESS_TOKEN=""
public_spaces=$(curl -s -H "Authorization: Bearer $MATRIX_ACCESS_TOKEN" "$MATRIX_SERVER/_matrix/client/r0/publicRooms")

if [ -z "${MATRIX_ACCESS_TOKEN}" ]; then
  echo error "You must provide an 'token'"
  exit 1
fi

exec > ./content/rooms/_index.md

echo "---
title: "Rooms/Spaces"
date: 2020-03-01T16:09:45-04:00
#headline: "The Community for Open Innovation and Collaboration"
#tagline: "The Eclipse Foundation provides our global community of individuals and organizations with a mature, scalable, and business-friendly environment for open source software collaboration and innovation."
hide_page_title: true
#hide_sidebar: true
#hide_breadcrumb: true
#show_featured_story: true
layout: "single"
#links: [[href: "/projects/", text: "Projects"],[href: "/org/workinggroups/", text: "Working Group"],[href: "/membership/", text: "Members"],[href: "/org/value", text: "Business Value"]]
#container: "container-fluid"
---
# Chat Service Exploration: Rooms & Spaces
Discover Eclipse Foundation's Chat Service by exploring rooms and spaces. Engage with projects members (project leads, commiters, contributors, ...), participate in discussion, and foster innovation within our vibrant community. 

Whether it's your first time here or if you need assistance, take a look at our [getting started!](https://chat.eclipse.org/docs/getting-started/) guide and our comprehensive [FAQ](https://chat.eclipse.org/docs/faq) to answer any questions.

{{< toc >}}
*last update: $(date +%m/%d/%Y)*
"

display_rooms() {
  local space_id="$1"
  rooms=$(curl -s -H "Authorization: Bearer $MATRIX_ACCESS_TOKEN" "$MATRIX_SERVER/_matrix/client/v1/rooms/$space_id/hierarchy")
  echo "${rooms//\\n/}" | jq -r '.rooms | sort_by(.name) | .[] | select(.children_state == [])  | select(.join_rule == "public") | .name, .room_id, .topic, .canonical_alias' | while read -r room_name; read -r room_id; read -r room_topic; read -r room_alias; do
    [[ ${room_name} == null ]] && continue; 
    [[ ${room_alias} == null ]] && continue; 
    echo "### ${room_name} Room {{< badge prefix=\"people\" text=\"$(get_members ${room_id})\" >}}"
     echo "**[${room_alias}](https://chat.eclipse.org/#/room/${room_alias})**"
    echo ""
    echo "$room_topic"
  done
}

get_rooms_number() {
  local space_id="$1"
  rooms=$(curl -s -H "Authorization: Bearer $MATRIX_ACCESS_TOKEN" "$MATRIX_SERVER/_matrix/client/v1/rooms/$space_id/hierarchy")
  echo "${rooms}" | jq -c '.rooms| map(select(.children_state == []))| map(select(.join_rule == "public"))|length'
}

get_members() {
  local room_id="$1"
  spaces_members=$(curl -s -H "Authorization: Bearer $MATRIX_ACCESS_TOKEN" "$MATRIX_SERVER/_matrix/client/r0/rooms/$room_id/members")
  echo "${spaces_members}" | jq '.chunk | map(select(.content.membership == "join")| select(.user_id|test("^(?!.*_bot).*$"))) | length'
}

echo "${public_spaces//\\n/}" | jq -r '.chunk' | jq 'sort_by(.name)' | jq -r '.[] | select(.room_type == "m.space") | .name, .room_id, .canonical_alias, .topic ' |  while read -r space_name; read -r space_id; read -r space_alias; read -r space_topic; do

  echo "_________________
  ## **$space_name Space** ${spaces_members_number} {{< badge theme="blue" prefix="room" text=\"$(get_rooms_number ${space_id})\" >}} {{< badge prefix="people" text=\"$(get_members ${space_id})\" >}}"
  echo "**[${space_alias}](https://chat.eclipse.org/#/room/${space_alias})**"
  echo ""
  echo "${space_topic}"

  display_rooms "$space_id"
done
